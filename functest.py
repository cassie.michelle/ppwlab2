from selenium import webdriver
import unittest
import time

class NewVisitorTest(unittest.TestCase):
    def setUp(self):
        self.browser = webdriver.Chrome()

    def tearDown(self): #
        self.browser.implicitly_wait(3)
        self.browser.quit()

    def test_can_start_a_list_and_retrieve_it_later(self):
        self.browser.get('http://cassieloveppw.herokuapp.com')
        time.sleep(5) # Let the user actually see something!
        title_box = self.browser.find_element_by_id('id_title')
        title_box.send_keys('Statusku')
        desc_box = self.browser.find_element_by_id('id_description')
        desc_box.send_keys('Ku pusing SDA dan pusing PPW kenapa hidup ku sulit sekali')
        submit_btn = self.browser.find_element_by_id('submit')

        submit_btn.submit()
        time.sleep(5) # Let the user actually see something!
        self.assertIn( "SDA", self.browser.page_source)
        time.sleep(10) # Let the user actually see something!

if __name__ == '__main__': #
    unittest.main(warnings='ignore') #
