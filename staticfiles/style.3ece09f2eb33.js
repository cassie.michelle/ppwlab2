$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});

(function ($) {
    'use strict';
    $('.item').on("click", function () {
        $(this).next().slideToggle(100);
        $('ul').not($(this).next()).slideUp('fast');
    });
}(jQuery));

$(document).ready(function(){
    $(".theme").click(function(){
        $(".title2").toggleClass('title3');
        $(".navbar-green").toggleClass('navbar-brown');
        $(".profile").toggleClass('profile2');
        $(".name").toggleClass('name1');
        $(".accordion").toggleClass('accordion2');
        $(".footer").toggleClass('footer2');
    });
});