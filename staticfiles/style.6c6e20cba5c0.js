$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});

(function ($) {
    'use strict';
    $('.item').on("click", function () {
        $(this).next().slideToggle(100);
        $('ul').not($(this).next()).slideUp('fast');
    });
}(jQuery));

$(document).ready(function(){
    var clicks = 0;
    $(".theme").click(function(){
        $(".title2").toggleClass('title3');
        $(".navbar-green").toggleClass('navbar-brown');
        $(".profile").toggleClass('profile2');
        $(".name").toggleClass('name1');
        $(".accordion").toggleClass('accordion2');
        $(".footer").toggleClass('footer2');
    });

    $(".btn-fav").click(function(){
        $(".btn-fav i").css('color:yellow');
    });
});

var myVar;

function myFunction() {
    myVar = setTimeout(showPage, 2000);
}

function showPage() {
  document.getElementById("spinner").style.display = "none";
  document.getElementById("myDiv").style.display = "block";
}

$(function(){
    var currPath = window.location.pathname; // Gets the current pathname ( /_display/ )

  if(currPath != '/profile'){ // Checks if the pathname equals the webpage you want the sidebar to be displayed on
    $('.theme').toggle(); // Set the sidebar to visibility: visible and display: none
  }else {}

})

$(document).ready(function(){
    $.ajax({
        method:"GET",
        url: "getJSON",
        success: function(result){
            var book_list = result.data;
            for(i = 0; i < book_list.length; i++){
                var title = book_list[i].title;
                // var name = title.replace(/'/g, "\\'");
                var button = '<button class="btn_fav btn btn-outline-light" onClick="addFavorite()" >' + 
                                '<i class="fa fa-star-o" style="color:orange; font-size:30px"></i></a>';
                var html = '<tr>' +
                             '<td>' + '<img src=' + '"' + book_list[i].image + '">' + '</td>' + 
                             '<td>' + title + '</td>' + 
                             '<td>' + book_list[i].authors + '</td>' + 
                             '<td>' + book_list[i].description + '</td>' + 
                             '<td>' + book_list[i].published + '</td>' + 
                             '<td>' + button + '</td>' + '</tr>';
                $('tbody').append(html);
            }
        },
        error: function(error){
            alert("Buku tidak ditemukan")
        }
    });
});

function addFavorite() {
    clicks++;
        document.getElementById("clicks").innerHTML = clicks;
};


