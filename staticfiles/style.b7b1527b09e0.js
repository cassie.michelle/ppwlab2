$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});

(function ($) {
    'use strict';
    $('.item').on("click", function () {
        $(this).next().slideToggle(100);
        $('ul').not($(this).next()).slideUp('fast');
    });
}(jQuery));

$(document).ready(function(){
    var clicks = 0;
    $(".theme").click(function(){
        $(".title2").toggleClass('title3');
        $(".navbar-green").toggleClass('navbar-brown');
        $(".profile").toggleClass('profile2');
        $(".name").toggleClass('name1');
        $(".accordion").toggleClass('accordion2');
        $(".footer").toggleClass('footer2');
    });

    $(".btn-fav").click(function(){
        $(".btn-fav i").css('color:yellow');
    });
});

var myVar;

function myFunction() {
    myVar = setTimeout(showPage, 2000);
}

function showPage() {
  document.getElementById("spinner").style.display = "none";
  document.getElementById("myDiv").style.display = "block";
}

$(function(){
    var currPath = window.location.pathname; // Gets the current pathname ( /_display/ )

  if(currPath != '/profile'){ // Checks if the pathname equals the webpage you want the sidebar to be displayed on
    $('.theme').toggle(); // Set the sidebar to visibility: visible and display: none
  }else {}

})

// $(document).ready(function(){
//     $.ajax({
//         method:"GET",
//         url: "getJSON",
//         success: function(result){
//             var book_list = result.data;
//             for(i = 0; i < book_list.length; i++){
//                 var title = book_list[i].title;
//                 var id = book_list[i].id;
//                 var button = '<img class="fav-btn" id="' + id + '" width="25" height="25" src="/static/default-star.png" onclick="addFavorite(id)">'
//                 var html = '<tr>' +
//                              '<td>' + '<img src=' + '"' + book_list[i].image + '">' + '</td>' + 
//                              '<td>' + title + '</td>' + 
//                              '<td>' + book_list[i].authors + '</td>' + 
//                              '<td>' + book_list[i].description + '</td>' + 
//                              '<td>' + book_list[i].published + '</td>' + 
//                              '<td>' + button + '</td>' + '</tr>';
//                 $('tbody').append(html);
//             }
//         },
//         error: function(error){
//             alert("Buku tidak ditemukan")
//         }
//     });
// });

// $("#search").click(function(){
//     var seach = $("input").val();
//     $.ajax({
//         method:"GET",
//         url: "getJSON",
//         data: {"search": seach},
//         success: function(result){
//             var book_list = result.data;
//             for(i = 0; i < book_list.length; i++){
//                 var title = book_list[i].title;
//                 var id = book_list[i].id;
//                 var button = '<img class="fav-btn" id="' + id + '" width="25" height="25" src="/static/default-star.png" onclick="addFavorite(id)">'
//                 var html = '<tr>' +
//                              '<td>' + '<img src=' + '"' + book_list[i].image + '">' + '</td>' + 
//                              '<td>' + title + '</td>' + 
//                              '<td>' + book_list[i].authors + '</td>' + 
//                              '<td>' + book_list[i].description + '</td>' + 
//                              '<td>' + book_list[i].published + '</td>' + 
//                              '<td>' + button + '</td>' + '</tr>';
//                 $('tbody').append(html);
//             }
//         },
//         error: function(error){
//             alert("Buku tidak ditemukan")
//         }
//     });
// });

function displayData(text){
    $.ajax({
        method:"GET",
        url: "getJSON",
        data: {"search":text},
        success: function(result){
            $('td').remove();
            var book_list = result.data;
            for(i = 0; i < book_list.length; i++){
                var title = book_list[i].title;
                var id = book_list[i].id;
                var button = '<img class="fav-btn" id="' + id + '" width="25" height="25" src="/static/default-star.png" onclick="addFavorite(id,'+ "'" + text + "'" + ')">'
                var html = '<tr>' +
                             '<td>' + '<img src=' + '"' + book_list[i].image + '">' + '</td>' + 
                             '<td>' + title + '</td>' + 
                             '<td>' + book_list[i].authors + '</td>' + 
                             '<td>' + book_list[i].description + '</td>' + 
                             '<td>' + book_list[i].published + '</td>' + 
                             '<td>' + button + '</td>' + '</tr>';
                $('tbody').append(html);
            }
        },
        error: function(error){
            alert("Buku tidak ditemukan")
        }
    });
}

function addFavorite(id,text) {
    $.ajax({
        method:"GET",
        url: "getJSON",
        data: {"search":text},
        success: function(result){
            var data = result.data;
            // count = document.getElementById('totalFav').innerHTML;
            // countFav = parseInt(count);
            // console.log(countFav);
            for (i = 0; i < data.length; i++) {
                var id2 = data[i].id;
                if (id == id2) {
                    var img = document.getElementById(id);
                    if (img.src.match("/static/default-star.png")) {
                        img.src = "/static/yellow-star.png";
                        clicks++;
                        document.getElementById("clicks").innerHTML = clicks;
                    } else {
                        img.src = "/static/default-star.png";
                        clicks--;
                        document.getElementById("clicks").innerHTML = clicks;
                    }
                }
            }
            // $('#totalFav').replaceWith('<span id="totalFav">' + countFav + '</span>');
        },
        error: function(error){
            alert("Books not found");
        }
    });
}

// function displayText(text) {
//     $.ajax({
//         method:"GET",
//         url: "getJSON",
//         success: function(result){
//             var book_list = result.data;
//             for(i = 0; i < book_list.length; i++){
//                 var title = book_list[i].title;
//                 var filter = book_list[i].title.toUpperCase();
//                 var id = book_list[i].id;
//                 var button = '<img class="fav-btn" id="' + id + '" width="25" height="25" src="/static/default-star.png" onclick="addFavorite(id)">'
//                 if (filter.search(text)> -1) {
//                     var html = '<tr>' +
//                              '<td>' + '<img src=' + '"' + book_list[i].image + '">' + '</td>' + 
//                              '<td>' + title + '</td>' + 
//                              '<td>' + book_list[i].authors + '</td>' + 
//                              '<td>' + book_list[i].description + '</td>' + 
//                              '<td>' + book_list[i].published + '</td>' + 
//                              '<td>' + button + '</td>' + '</tr>';
//                     $('tbody').append(html);
//                 }                
//             }
//         },
//         error: function(error){
//             alert("Buku tidak ditemukan")
//         }
//     });
// };





