$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});

(function ($) {
    'use strict';
    $('.item').on("click", function () {
        $(this).next().slideToggle(100);
        $('ul').not($(this).next()).slideUp('fast');
    });
}(jQuery));

$(document).ready(function(){
    var clicks = 0;
    $(".theme").click(function(){
        $(".title2").toggleClass('title3');
        $(".navbar-green").toggleClass('navbar-brown');
        $(".profile").toggleClass('profile2');
        $(".name").toggleClass('name1');
        $(".accordion").toggleClass('accordion2');
        $(".footer").toggleClass('footer2');
    });

    $(".btn-fav").click(function(){
        $(".btn-fav i").css('color:yellow');
    });

});

var myVar;

function myFunction() {
    myVar = setTimeout(showPage, 2000);
}

function showPage() {
  document.getElementById("spinner").style.display = "none";
  document.getElementById("myDiv").style.display = "block";
}

$(function(){
    var currPath = window.location.pathname; // Gets the current pathname ( /_display/ )

  if(currPath != '/profile'){ // Checks if the pathname equals the webpage you want the sidebar to be displayed on
    $('.theme').toggle(); // Set the sidebar to visibility: visible and display: none
  }else {}

})

function displayData(text){
    $.ajax({
        method:"GET",
        url: "getJSON",
        data: {"search":text},
        success: function(result){
            $('td').remove();
            var book_list = result.data;
            for(i = 0; i < book_list.length; i++){
                var title = book_list[i].title;
                var id = book_list[i].id;
                var button = '<img class="fav-btn" id="' + id + '" width="25" height="25" src="/static/default-star.png" onclick="addFavorite(id,'+ "'" + text + "'" + ')">'
                var html = '<tr>' +
                             '<td>' + '<img src=' + '"' + book_list[i].image + '">' + '</td>' + 
                             '<td>' + title + '</td>' + 
                             '<td>' + book_list[i].authors + '</td>' + 
                             '<td>' + book_list[i].description + '</td>' + 
                             '<td>' + book_list[i].published + '</td>' + 
                             '<td>' + button + '</td>' + '</tr>';
                $('tbody').append(html);
            }
        },
        error: function(error){
            alert("Buku tidak ditemukan")
        }
    });
}

function addFavorite(id,text) {
    $.ajax({
        method:"GET",
        url: "getJSON",
        data: {"search":text},
        success: function(result){
            var data = result.data;
            // count = document.getElementById('totalFav').innerHTML;
            // countFav = parseInt(count);
            // console.log(countFav);
            for (i = 0; i < data.length; i++) {
                var id2 = data[i].id;
                if (id == id2) {
                    var img = document.getElementById(id);
                    if (img.src.match("/static/default-star.png")) {
                        img.src = "/static/yellow-star.png";
                        clicks++;
                        document.getElementById("clicks").innerHTML = clicks;
                    } else {
                        img.src = "/static/default-star.png";
                        clicks--;
                        document.getElementById("clicks").innerHTML = clicks;
                    }
                }
            }
            // $('#totalFav').replaceWith('<span id="totalFav">' + countFav + '</span>');
        },
        error: function(error){
            alert("Books not found");
        }
    });
}

// story 10
$(function () {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    var email_available;
    var timer = 0;

    $('#email').keydown(function () {
        clearTimeout(timer);
        timer = setTimeout(checkValidEmail, 1000);
        warning();
    });

    $('#name').keydown(function () {
        clearTimeout(timer);
        timer = setTimeout(warning, 1000);
    });

    $('#password').keydown(function () {
        clearTimeout(timer);
        timer = setTimeout(warning, 1000);
    });

    $('#form').on('submit', function (event) {
        event.preventDefault();
        console.log("Form Submitted!");
        saveData();
    });

    function saveData() {
        $.ajax({
            method: 'POST',
            url: "add_user",
            headers: {
                "X-CSRFToken": csrftoken,
            },
            data: {
                name: $('#name').val(),
                email: $('#email').val(),
                password: $('#password').val(),
            },
            success: function (response) {
                if (response.is_success) {
                    $('#name').val('');
                    $('#email').val('');
                    $('#password').val('');
                    $('#submit-btn').prop('disabled', true);
                    $('.error-message p').replaceWith("<p class='success'>You have successfully subscribed!</p>");
                    console.log("Successfully add data");
                } else {
                    $('.error-message p').replaceWith("<p class='fail'>Oops! Something went wrong!</p>");
                }
            },
            error: function () {
                alert("Error, cannot save data to database");
            }
        })
    }

    function checkValidEmail() {
        var reg = /^([a-zA-Z0-9_.+-])+@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var is_valid = reg.test($('#email').val());
        if (is_valid) {
            validateEmail();
        } else {
            $('.error-message p').replaceWith("<p class='fail'>Please enter a valid email format!</p>");
        }
    }

    function validateEmail() {
        $.ajax({
            method: 'POST',
            url: "validate_email",
            headers: {
                "X-CSRFToken": csrftoken,
            },
            data: {
                email: $('#email').val(),
            },
            success: function (email) {
                if (email.is_exists) {
                    email_available = false;
                    $('.error-message p').replaceWith("<p class='fail'>Please use another email! This email is already been registered.</p>");
                } else {
                    email_available = true;
                    warning();
                }
            },
            error: function () {
                alert("Error, cannot validate email!")
            }
        })
    }

    function warning() {
        var password = $('#password').val();
        var name = $('#name').val();
        var email = $('#email').val();
        if (password.length !== 0 && name.length !== 0 && email_available) {
            $('.error-message p').replaceWith("<p></p>");
            $('#submit-btn').prop('disabled', false);
        } else if (password.length === 0 && name.length === 0) {
            $('#submit-btn').prop('disabled', true);
            $('.error-message p').replaceWith("<p class='fail'>Name and password cannot be empty</p>");
        } else if (password.length === 0 && email.length === 0) {
            $('#submit-btn').prop('disabled', true);
            $('.error-message p').replaceWith("<p class='fail'>Email and password cannot be empty</p>");
        } else if (name.length === 0 && email.length) {
            $('#submit-btn').prop('disabled', true);
            $('.error-message p').replaceWith("<p class='fail'>Name and email cannot be empty</p>");
        } else if (password.length === 0) {
            $('#submit-btn').prop('disabled', true);
            $('.error-message p').replaceWith("<p class='fail'>Password cannot be empty</p>");
        } else if (password.length < 6 && password.length > 12) {
            $('#submit-btn').prop('disabled', true);
            $('.error-message p').replaceWith("<p class='fail'>Password must contain 6-12 characters</p>");
        } else if (name.length === 0) {
            $('#submit-btn').prop('disabled', true);
            $('.error-message p').replaceWith("<p class='fail'>Name cannot be empty</p>");
        } else if (email.length === 0) {
            $('#submit-btn').prop('disabled', true);
            $('.error-message p').replaceWith("<p class='fail'>Email cannot be empty</p>");
        } else {
            $('#submit-btn').prop('disabled', true);
            $('.error-message p').replaceWith("<p class='fail'>Please enter a valid email format!</p>");
        }
    }
});

// function displaySubs(){
//     $.ajax({
//         method:"GET",
//         url: "getSubs",
//         success: function(result){
//             var subs_list = result.subs;
//             for(i = 0; i < subs_list.length; i++){
//                 var email = subs_list[i].email;
//                 var name = subs_list[i].name;
//                 var button = '<button class="unsubs-btn" id="' + email + '" width="25" height="25" onclick="unSubscribe(email)">Unsubscribe</button>'
//                 var html = '<tr>' +
//                              '<td>' + name + '</td>' + 
//                              '<td>' + email + '</td>' + 
//                              '<td>' + button + '</td>' + '</tr>';
//                 $('tbody').append(html);
//             }
//         },
//         error: function(error){
//             alert("Subscriber not found")
//         }
//     });
// }









