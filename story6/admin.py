from django.contrib import admin
from .models import Status, Book, Regist
# Register your models here.

admin.site.register(Status)
admin.site.register(Book)
admin.site.register(Regist)
