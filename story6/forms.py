from django import forms

class Status_Form(forms.Form):
    error_messages = {
        'required': 'Please fill this field',
    }
    title_attrs = {
        'type': 'text',
        'class': 'status-form-input form-control',
        'placeholder':'Input the title'
    }
    description_attrs = {
        'type': 'text',
        'class': 'status-form-textarea form-control',
        'placeholder':'Input the description'
    }

    title = forms.CharField(label='', required=True, max_length=27, widget=forms.TextInput(attrs=title_attrs))
    description = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=description_attrs))

class Regist_Form(forms.Form):
    error_messages = {
        'required': 'Please fill this field',
    }
    username_attrs = {
        'type': 'text',
        'class': 'status-form-input form-control',
        'id': 'name',
        'placeholder':'Your username'
    }
    email_attrs = {
        'type': 'email',
        'class': 'status-form-input form-control',
        'id':'email',
        'placeholder':'Your e-mail'
    }
    password_attrs = {
        'type': 'password',
        'class': 'status-form-input form-control',
        'id':'password',
        'placeholder':'Your password'
    }
    name = forms.CharField(label='Username',required = True, widget=forms.TextInput(attrs=username_attrs))
    email = forms.EmailField(label='E-mail',required = True, widget=forms.EmailInput(attrs=email_attrs))
    password = forms.CharField(label='Password',required = True, widget=forms.TextInput(attrs=password_attrs)) 

