from django.db import models

# Create your models here.

class Status(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField()
    created_date = models.DateTimeField(auto_now_add=True)

class Book(models.Model):
    book_title = models.CharField(max_length=100)
    book_description = models.TextField()
    created_date = models.DateTimeField(auto_now_add=True)

class Regist(models.Model):
    name = models.CharField(max_length=30)
    email = models.EmailField(unique=True)
    password = models.CharField(max_length=27)

    def as_dict(self):
        return{
            "name": self.name,
            "email": self.email,
        }
