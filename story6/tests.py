from django.db import IntegrityError
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, add_status, profile, registrasi, add_user
from .models import Status, Regist
from .forms import Status_Form, Regist_Form

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import unittest
import time

# Create your tests here.
class Story6UnitTest(TestCase):
    def test_story6_url_is_exist(self):
        response = Client().get('/story6/')
        self.assertEqual(response.status_code, 200)

    def test_story6_using_index_func(self):
        found = resolve('/story6/')
        self.assertEqual(found.func, profile)

    def test_model_can_add_new_status(self):
        title = Status.objects.create(title='mengerjakan lab ppw', description='mengerjakan lab_5 ppw')
    
        # Retrieving all available activity
        counting_all_available_status = Status.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)

    def test_form_validation_for_blank_items(self):
        form = Status_Form(data={'title': '', 'description': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['description'],
            ["This field is required."]
        )

    def test_form_todo_input_has_placeholder_and_css_classes(self):
        form = Status_Form()
        self.assertIn('class="status-form-input', form.as_p())
        self.assertIn('id="id_title"', form.as_p())
        self.assertIn('class="status-form-textarea', form.as_p())
        self.assertIn('id="id_description', form.as_p())

    def test_story6_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/story6/add_status', {'title': test, 'description': test})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/story6/index/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_story6_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/story6/add_status', {'title': '', 'description': ''})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/story6/index/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_story6_using_profile_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, profile)

    def test_story6_url_profile_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_story6_profile_render_the_result(self):
        test = 'Student of Information System'
        response= Client().get('/profile/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_story6_url_library_is_exist(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)
        
    # def test_client_can_get_JSON(self):
    #     response = Client().get('/getJSON/')
    #     self.assertEqual(response.status_code, 200)

    #TDD story 10
    def test_subcribe_page_url_is_exist(self):
        response = Client().get('/registrasi/')
        self.assertEqual(response.status_code, 200)
    
    def test_subscribe_using_function(self):
        found = resolve('/story6/registrasi/')
        self.assertEqual(found.func, registrasi)

    def test_model_can_save_data_form(self):
        data = Regist.objects.create(name="me", email="me@gmail.com", password="me123")
        counting_all_data = Regist.objects.all().count()
        self.assertEqual(counting_all_data, 1)
    
    def test_self_func_name(self):
        Regist.objects.create(name="me", email="me@gmail.com", password="me123")
        subscriber = Regist.objects.get(email='me@gmail.com')
        self.assertEqual('me@gmail.com', subscriber.email)

    def test_email_must_unique(self):
        Regist.objects.create(email="me@gmail.com")
        with self.assertRaises(IntegrityError):
            Regist.objects.create(email="me@gmail.com")

    def test_post_using_ajax(self):
        response = Client().post('/add_user/', data={
            "name": "me",
            "email": "me@gmail.com",
            "password": "me123",
        })
        self.assertEqual(response.status_code, 200)

    def test_check_email_view_return_200(self):
        response = Client().post('/validate_email/', data={
            "email": "me@gmail.com"
        })
        self.assertEqual(response.status_code, 200)
    
    def test_check_email_already_exist_view_return_200(self):
        Regist.objects.create(name="me", email="me@gmail.com", password="me123")
        response = Client().post('/validate_email/', data={
            "email": "me@gmail.com"
        })
        self.assertEqual(response.json()['is_exists'], True)

    def test_if_subform_is_blank(self):
        form = Regist_Form(data={
            "name": "",
            "email": "",
            "password": "",
        })
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['name'][0],
            'This field is required.',
        )

    def test_subform_todo_input_has_placeholder_and_css_classes(self):
        form = Regist_Form()
        self.assertIn('class="status-form-input form-control', form.as_p())

    def test_subcriber_page_url_is_exist(self):
        response = Client().get('/subscriber/')
        self.assertEqual(response.status_code, 200)
        
# class NewVisitorTest(unittest.TestCase):
#     def setUp(self):
#         capabilities = {
#             'browserName': 'chrome',
#             'chromeOptions': {
#                 'useAutomationExtension': False,
#                 'forceDevToolsScreenshot': True,
#                 'args': ['--headless', '--no-sandbox','disable-dev-shm-usage']
#             }
#         }
#         self.browser = webdriver.Chrome('./chromedriver', desired_capabilities=capabilities)

#     def tearDown(self):
#         self.browser.quit()

    # def test_can_input_status_title_desc(self):
    #     self.browser.get('http://cassieloveppw.herokuapp.com')
    #     # time.sleep(5) # Let the user actually see something!
    #     title_box = self.browser.find_element_by_id('id_title')
    #     title_box.send_keys('cobacoba')
    #     desc_box = self.browser.find_element_by_id('id_description')
    #     desc_box.send_keys('Ku pusing SDA dan pusing PPW kenapa hidup ku sulit sekali')
    #     submit_btn = self.browser.find_element_by_id('submit')
    #     submit_btn.submit()
    #     # time.sleep(5) # Let the user actually see something!
    #     self.assertIn("cobacoba", self.browser.page_source)
    #     self.assertIn("Ku pusing SDA dan pusing PPW kenapa hidup ku sulit sekali", self.browser.page_source)
    #     # time.sleep(10) # Let the user actually see something!

    # def test_css_for_header(self):
    #     self.browser.get('http://cassieloveppw.herokuapp.com')
    #     header_box = self.browser.find_element_by_tag_name("header")
    #     size = header_box.size
    #     height = size['height']
    #     width = size['width']
    #     self.assertEqual(220,height)
    #     # self.assertEqual(785,width)
    #     # self.assertEqual({'height': 220, 'width': 774}, size)

    # def test_css_for_footer(self):
    #     self.browser.get('http://cassieloveppw.herokuapp.com')
    #     footer_css = self.browser.find_element_by_tag_name('footer')
    #     size = footer_css.size
    #     height = size['height']
    #     width = size['width']
    #     self.assertEqual(401,height)
    #     # self.assertEqual(785,width)

    # def test_header_position(self):
    #     self.browser.get('http://cassieloveppw.herokuapp.com')
    #     header_box = self.browser.find_element_by_tag_name("header")
    #     position = header_box.location
    #     x = position['x']
    #     y = position['y']
    #     self.assertEqual(0,x)
    #     self.assertEqual(56,y)
       
    # def test_footer_position(self):
    #     self.browser.get('http://cassieloveppw.herokuapp.com')
    #     footer_box = self.browser.find_element_by_tag_name("footer")
    #     position = footer_box.location
    #     x = position['x']
    #     # y = position['y']
    #     self.assertEqual(0,x)
    #     # self.assertEqual(17645,y) //posisi y nya tidak pasti

        
        



    





