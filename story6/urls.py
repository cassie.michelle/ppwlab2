from django.conf.urls import url
from .views import index, add_status, add_user
from .views import registrasi, add_user, validate_email
from .views import subscriber, delete_subs, subs_list_json
from .views import book, getJSON
from .views import profile
from .views import login

urlpatterns = [
    url('index', index, name='index'),
    url('add_status', add_status, name='add_status'),

    url('profile', profile, name='profile'),

    url('book', book, name='book'),
    url('getJSON', getJSON, name='getJSON'),

    url('registrasi', registrasi, name='registrasi'),
    url('add_user', add_user, name='add_user'),
    url('validate_email', validate_email, name='validate_email'),
    
    url('subscriber', subscriber, name='subscriber'),
    url('subs_list_json', subs_list_json, name='subs_list_json'),
    url('delete_subs', delete_subs, name='delete_subs'),

    url('login', login, name='login'),
    url('', profile)

]