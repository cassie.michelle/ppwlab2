from django.http import JsonResponse
from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.csrf import csrf_protect
import requests

from google.oauth2 import id_token
from google.auth.transport import requests as request2
from django.http import HttpResponseRedirect
from django.urls import reverse

from .models import Status, Book, Regist
from .forms import Status_Form, Regist_Form

# Create your views here.
response = {'author': "Cassie Michelle"}

#Render status.html
def index(request) :
    status = Status.objects.all()
    response['status'] = status
    response['status_form'] = Status_Form
    html = 'index.html'
    return render(request, html , response)

def add_status(request):
    # form = Status_Form(request.POST or None)
    if(request.method == 'POST'):
        response['title'] = request.POST['title']
        response['description'] = request.POST['description']
        status = Status(title=response['title'],description=response['description'])
        status.save()
    return HttpResponseRedirect('/story6/')

#Render profile.html
def profile(request) :
    return render(request, 'profile.html' ,response)


#Render regist.html (subscriber)
def registrasi(request) :
    form = Regist_Form(request.POST or None)
    response['form'] = form
    return render(request, 'registrasi.html', response)

def add_user(request) :
    form = Regist_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        name = request.POST['name']
        email = request.POST['email']
        password = request.POST['password']
        status = True
        try:
            Regist.objects.create(name=name, email=email, password=password)
        except:
            status = False
        return JsonResponse({'is_success':status})

@csrf_exempt
def validate_email(request):
    if request.method == 'POST':
        email = request.POST['email']
        check_email = Regist.objects.filter(email=email)
        if check_email.exists():
            return JsonResponse({'is_exists': True})
        return JsonResponse({'is_exists': False})

#Render subscriber.html

def subscriber(request) :
    subs_list = Regist.objects.all()
    response['subs_list'] = subs_list
    return render(request, 'subscriber.html', response)

# def getSubs(request):
#     subs = Regist.objects.all()
#     newResult = []
#     for items in subs:
#         newDict = {"nama":items.nama,"email":items.email}
#         newResult.append(newDict)
#     return JsonResponse({'subs': newResult})

def subs_list_json(request) :
    subs = [obj.as_dict() for obj in Regist.objects.all()]
    return JsonResponse ({"results" : subs}, content_type = 'application/json')

@csrf_exempt
def delete_subs(request) :
    if request.method == 'POST':
        email = request.POST['email']
        Regist.objects.filter(email=email).delete()
        subs = [obj.as_dict() for obj in Regist.objects.all()]
        return JsonResponse ({"results" : subs}, content_type = 'application/json')

#Render book.html
def book(request) :
    if 'user_id' not in request.session:
        return HttpResponseRedirect(reverse('login'))
    if request.method == 'POST':
        id = request.POST['id']
        if not 'book' in request.session.keys():
            request.session['book'] = [id]
            size = 1
        else:
            books = request.session['book']
            books.append(id)
            request.session['book'] = books
            size = len(books)
        return JsonResponse({'count': size, 'id': id})
    if 'book' in request.session.keys():
        listbookSession = request.session['book']
    else:
        listbookSession = []
    nama = request.session['name']
    return render(request, 'book.html', {'count': len(listbookSession), 'nama': nama })


def getJSON(request):
    search = request.GET.get('search')

    if 'user_id' not in request.session:
        return HttpResponseRedirect(reverse('login'))

    if(search == None):
        get = requests.get("https://www.googleapis.com/books/v1/volumes?q=quilting").json()
    else:
        get = requests.get("https://www.googleapis.com/books/v1/volumes?q="+ search).json()
    
    result = get['items']
    newResult = [] 
    
    for items in result:
        idData = items['id']
        if idData in request.session['book']:
            bool = True
        else:
            bool = False
        newDict = {"title": items['volumeInfo']['title'], "authors": items['volumeInfo']['authors'],
                   "published": items['volumeInfo']['publishedDate'], "image": items['volumeInfo']['imageLinks']['thumbnail'], 
                   "description": items['volumeInfo']['description'], 'id': items['id'], 'boolean': bool }
        newResult.append(newDict)
    return JsonResponse({'data': newResult})

# Render login.html   
@csrf_exempt  
def login(request):
    if request.method == "POST":
        try:
            # name_g = request.POST['id_profile']
            token = request.POST['id_token']
            idinfo = id_token.verify_oauth2_token(token, request2.Request(),
                                                  "783400298974-jl95sm2i2gpjjelm8r049u3tubqhbc7m.apps.googleusercontent.com")
            if idinfo['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
                raise ValueError('Wrong issuer.')

            userid = idinfo['sub']
            email = idinfo['email']
            name = idinfo['name']
            request.session['user_id'] = userid
            request.session['email'] = email
            request.session['name'] = name
            request.session['book'] = []
            # response['google_name'] = name_g

            return JsonResponse({"status": "0", "url": reverse('book')})
        except ValueError:
            return JsonResponse({"status": "1"})
    return render(request, 'login.html')

def logout(request):
    request.session.flush()
    return HttpResponseRedirect(reverse('login'))




